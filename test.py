#!/usr/bin/env python
from flask import Flask
import datetime
from flask import render_template
from wtforms import Form, Field
from wtforms import StringField, PasswordField, BooleanField, SubmitField, DecimalField, DateTimeField, SelectField, RadioField
from wtforms.validators import DataRequired
from wtforms.widgets import TextInput
from elasticsearch import Elasticsearch
from elasticsearch.helpers import scan, bulk
import json
import os

app = Flask(__name__)
app.config['SECRET_KEY'] = 'whatever'

class TagListField(Field):
    widget = TextInput()

    def _value(self):
        if self.data:
            return u', '.join(self.data)
        else:
            return u''

    def process_formdata(self, valuelist):
        if valuelist:
            self.data = [x.strip() for x in valuelist[0].split(',')]
        else:
            self.data = []


class CommonForm(Form):
    ipv6 = BooleanField('IPv6')
    hrs = DecimalField('Hours Range', validators=[DataRequired()])
    src = TagListField('Source(s)', validators=[DataRequired()])
    dest = TagListField('Destination(s)', validators=[DataRequired()])
    datetime = DateTimeField('Time', validators=[DataRequired()])
    submit = SubmitField('Submit')
    
class SymmetryForm(Form): #could probably merge this and CommonForm
    ipv6 = BooleanField('IPv6')
    hrs = DecimalField('Hours Range', validators=[DataRequired()])
    src = StringField('Source')
    dest = StringField('Destination', validators=[DataRequired()])
    datetime = DateTimeField('Time', validators=[DataRequired()])
    submit = SubmitField('Submit', validators=[DataRequired()])

class ContainingForm(Form):
    address = StringField('IP Address', validators=[DataRequired()])
    dt_from = DateTimeField('From', validators=[DataRequired()])
    dt_to = DateTimeField('To', validators=[DataRequired()])
    submit = SubmitField('Submit')
    
class UniqueForm(Form):
    ipv6 = BooleanField('IPv6')
    src = StringField('Source', validators=[DataRequired()])
    dest = StringField('Destination', validators=[DataRequired()])
    dt_from = DateTimeField('From', validators=[DataRequired()])
    dt_to = DateTimeField('To', validators=[DataRequired()])
    metric = SelectField('Metric', choices=[('pl', 'packet loss'), ('owd', 'one way delay'), ('tp', 'throughput'), ('rt', 'retransmits')])
    submit = SubmitField('Submit')


srcs = ["src1", "another", "placeholder"]
dests = ["dest1", "2_dest", "placeholder"]
ips = ["0.0.0.0", "127.0.0.1", "192.55.7.3"]

@app.route("/")
@app.route("/about")
def about():
    return render_template('about.html')
@app.route("/common")
def common():
    form = CommonForm()
    """
    user = None
    passwd = None
    if user is None and passwd is None:
        with open("creds.key") as f:
            user = f.readline().strip()
            passwd = f.readline().strip()
    credentials = (user, passwd)
    es = Elasticsearch(['192.170.226.230:9200'], timeout=240, http_auth=credentials)
    if es.ping() == True:
        return "aaa"
    return "yeet"
    querys = {
        "size": 0,
          "query": {
            "bool": {
              "filter": [
                {
                  "term": {
                    "destination_reached": {
                      "value": True
                    }
                  }
                },
                {
                  "term": {
                    "path_complete": {
                      "value": True
                    }
                  }
                }
              ]
            }
          },
            "aggs": {
                "srcs": {
                  "terms": {
                    "field": "src_host",
                    "size": 500
                  }
                }
              }
            }
    datas = es.search(index='ps_trace', body=querys)
    return "yeet2"
    src_arr = []
    for x in datas['aggregations']['srcs']['buckets']:
        src_arr.append(x['key'])
    """
    return render_template('common.html', form=form, srcs=srcs, dests=dests)
@app.route("/symmetry")
def symmetry():
    form = SymmetryForm()
    return render_template('symmetry.html', form=form, srcs=srcs, dests=dests)
@app.route("/containing")
def containing():
    form = ContainingForm()
    return render_template('containing.html', form=form, ips=ips)
@app.route("/unique")
def unique():
    form = UniqueForm()
    return render_template('unique.html', form=form, srcs=srcs, dests=dests)

if __name__ == "__main__":
    app.run(host='0.0.0.0')

